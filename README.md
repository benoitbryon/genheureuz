# La GéNHEUREUZ, une monnaie inversée ?

La GéNHEUREUZ est l'expérimentation d'une monnaie "inversée",
où un billet accompagne quelque chose qu'on offre, en tant que témoignage du cadeau,
plutôt que comme rétribution de ce qui est reçu.

Un billet de GéNHEUREUZ :

* ne confère aucun pouvoir d'achat !
* témoigne d'un don effectué, pas d'une promesse ni d'un crédit ;
* explicite la valeur de ce qui est offert : du temps, de l'attention, etc.
* invite à propager le geste ;
* ne fait sens que lorsqu'il circule.

Ce dépôt contient le code source qui permet d'éditer des planches de GéNHEUREUZ.

Vous trouverez les planches à imprimer *prêtes à l'emploi* au format PDF dans les [releases](https://framagit.org/benoitbryon/genheureuz/-/releases) du projet.
Par exemple pour la version 1.0 :

* [planches 1 billet par page](https://framagit.org/benoitbryon/genheureuz/-/blob/1.0/build/banknotes-singles.pdf) ;
* [planches A3 avec 10 billets pour chaque recto-verso](https://framagit.org/benoitbryon/genheureuz/-/blob/1.0/build/banknotes-boards.pdf), à imprimer et découper ;
* [autres fichiers](https://framagit.org/benoitbryon/genheureuz/-/blob/1.0/build/)...


## Concept

![Sketchnote présentant le concept](concept-scene.jpg)

Cette "monnaie" a été créée pour accompagner la mise en place de temps d'écoute
au sein d'un collectif. N'hésitez pas à partager vos propres histoires !


## Exemple

![Prototypes de "billets"](samples/1.jpg)


## Licence et crédits

**Ce projet est fourni dans l'intention de faciliter l'utilisation et l'expérimentation du concept. Bienvenue aux créations, améliorations et détournements qu'il peut inspirer !**

**Conception et réalisation originale :** [Benoît Bryon](https://marmelune.net).
Le design fourni ici et son code source sont publiés **sous licence Creative Commons [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)**.
C'est un cadeau destiné à être offert à son tour ;)
Pas d'utilisation commerciale !

Les **photos** utilisées dans le design original sont issues de la banque d'images libres [Unsplash](https://unsplash.com).
Photographes :

* [Zosia Korcz](https://unsplash.com/@calanthe),
* [Ray Hennessy](https://unsplash.com/@rayhennessy),
* [Christopher Previte](https://unsplash.com/@cprevite),
* [Ian Baldwin](https://unsplash.com/@ianebaldwin),
* [frdm](https://unsplash.com/@frdm),
* [Hansjörg Keller](https://unsplash.com/@kel_foto),
* [Derek Oyen](https://unsplash.com/@goosegrease),
* [Jonatan Pie](https://unsplash.com/@r3dmax).

**Polices :**

* [Londrina Solid](https://fonts.google.com/specimen/Londrina+Solid) & [Londrina Outline](https://fonts.google.com/specimen/Londrina+Outline) by Marcelo Magalhães, Open Font Licence
* [Lovers Quarrel](https://fonts.google.com/specimen/Lovers+Quarrel) by TypeSETit, Open Font License

**Logiciels :**

* [Inkscape](https://inkscape.org/) pour les fichiers .svg et .svgz
* [GIMP](https://www.gimp.org/) pour les fichiers .xcf
